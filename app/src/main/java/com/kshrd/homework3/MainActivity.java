package com.kshrd.homework3;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText edTitle,edDescription;
    private TextView textDefault;
    private Button btnSave,btnCancel;
    private LinearLayout frmAddTask;
    private Animation animation;
    private LinearLayout mainList;
    private LinearLayout subList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainList = findViewById(R.id.mainList);
        edTitle = findViewById(R.id.edTitle);
        edDescription = findViewById(R.id.edDescription);
        btnCancel = findViewById(R.id.btnCancel);
        btnSave = findViewById(R.id.btnSave);
        frmAddTask = findViewById(R.id.frmAddTask);

        animation = AnimationUtils.loadAnimation(this, R.anim.slide_down);
        animation.setDuration(0);
        frmAddTask.startAnimation(animation);

        textDefault = findViewById(R.id.defaultTextView);

        textDefault.setText("No To Do List");


        btnSave.setOnClickListener(v -> {
            String title = edTitle.getText().toString();
            String description = edDescription.getText().toString();
            this.createList(title,description);

            if(mainList.getChildCount() > 0){
                textDefault.setText("");
            }

            getSupportActionBar().show();

        });
        btnCancel.setOnClickListener(v -> {
            this.closeFrom();
            getSupportActionBar().show();
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.my_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case R.id.mAddTask:
                loadForm();
                getSupportActionBar().hide();
                return true;
            case R.id.mFadeIn:
                setAnimate(R.anim.fade_in);
                return true;
            case R.id.mFadeOut:
                setAnimate(R.anim.fade_out);
                return true;
            case R.id.mZoom:
                setAnimate(R.anim.zoom);
                return true;
            case R.id.mRotate:
                setAnimate(R.anim.rotate);
                return true;
            case R.id.mRemoveList:
                if(mainList.getChildCount() > 0){
                    mainList.removeAllViews();
                    textDefault.setText("No To Do List");
                }
                return true;
            default: return super.onOptionsItemSelected(item);
        }

    }

    public void setAnimate(int animate){
        Intent intent = new Intent(this,AnimationActivity.class);
        intent.putExtra("anim",animate);
        startActivity(intent);
    }

    public void createList(String title,String description){

        subList = new LinearLayout(this);
        LinearLayout.LayoutParams mainParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        mainParams.setMargins(0,0,0,getResources().getDimensionPixelSize(R.dimen.m_2));

        LinearLayout.LayoutParams sublistParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT );

        int pd4 = getResources().getDimensionPixelSize(R.dimen.pd_10);
        subList.setOrientation(LinearLayout.VERTICAL);
        subList.setPadding(pd4,pd4,pd4,pd4);
        subList.setBackgroundColor(Color.WHITE);
        subList.setLayoutParams(sublistParams);


        TextView titleText = new TextView(this);
        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT );
        titleText.setText(title);
        titleText.setMaxLines(1);
        titleText.setEllipsize(TextUtils.TruncateAt.END);
        titleText.setTextSize(20);
        titleText.setTypeface(null, Typeface.BOLD);

        TextView descriptionText = new TextView(this);
        LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT );
        descriptionText.setText(description);
        descriptionText.setMaxLines(2);
        descriptionText.setEllipsize(TextUtils.TruncateAt.END);

        subList.addView(titleText,params1);
        subList.addView(descriptionText,params2);
        mainList.addView(subList,mainParams);
        this.clearInput();
    }

    public void loadForm(){
        animation = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.slide_up);
        frmAddTask.startAnimation(animation);
        edTitle.requestFocus();
    }

    public void clearInput(){
        edTitle.setText("");
        edDescription.setText("");
        animation = AnimationUtils.loadAnimation(this,R.anim.slide_down);
        frmAddTask.startAnimation(animation);
        InputMethodManager imm = (InputMethodManager)getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edDescription.getWindowToken(), 0);
    }


    public void closeFrom(){
        animation = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.slide_down);
        frmAddTask.startAnimation(animation);
    }
}