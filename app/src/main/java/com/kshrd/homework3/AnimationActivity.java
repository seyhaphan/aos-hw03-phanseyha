package com.kshrd.homework3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

public class AnimationActivity extends AppCompatActivity {

    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animation);
        imageView = findViewById(R.id.imageView);

        Intent intent = getIntent();
        int animate = intent.getIntExtra("anim",0);
        Toast.makeText(this,animate,Toast.LENGTH_SHORT);
        Animation animation = AnimationUtils.loadAnimation(this,animate);
        imageView.startAnimation(animation);
    }
}